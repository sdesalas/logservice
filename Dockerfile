FROM node:20.8.1-slim
WORKDIR /var/task
COPY ./package* ./
RUN npm ci --arch=x64 --platform=linux
COPY . .
RUN npm run build
CMD ["node", "dist/server.js"]
