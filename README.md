# Logservice

A logging service using both GraphQL and RESTful endpoints

## Getting started

Please make sure you have previously installed Nodejs v20.8.1.

```sh
$ node -v
v20.8.1

$ npm -v
10.1.0
```

If that is the case, make sure you install the `node_modules` required for the project

```sh
$ npm i
added 133 packages, and audited 134 packages in 1s

14 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```

## Development

```sh
$ npm run dev

> logservice@1.0.0 dev
> nodemon server.ts

[nodemon] 3.0.1
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: ts,json
[nodemon] starting `ts-node server.ts`
Running GraphQL API endpoint at http://localhost:4000/graphql
Running RESTful API endpoint at http://localhost:4000/rest
```

## Testing

GraphQL API testing via command line `curl` client

```sh
$ curl -X POST -H 'Content-Type: application/json' --data '{"query": "{ saveLogs (logs: [\"some log data\", \"some more log data\"]) }"}' http://localhost:4000/graphql
{"data":{"saveLogs":["some log data","some more log data"]}}
$ curl -X POST -H 'Content-Type: application/json' --data '{"query": "{ getLogs }"}' http://localhost:4000/graphql
{"data":{"getLogs":["some log data","some more log data"]}}
```

RESTful API testing via command line `curl` client

```sh
$ curl -X POST --data $'some log data\nsome more log data' http://localhost:4000/rest/logs
some log data
some more log data
$ curl http://localhost:4000/rest/logs
some log data
some more log data
```

## Building the project

The project will build on the `dist/` directory. The main file for the project is `dist/server.js`


```sh
$ npm run build

> logservice@1.0.0 build
> tsc
```

## Building and running inside a container

To test deployment to kubernetes, we need to check the docker container is working properly

```sh
$ docker build -t logservice .
 => [internal] load build context                                                                                  1.8s
 => => transferring context: 53.23MB                                                                               1.7s
 => [2/6] WORKDIR /var/task                                                                                        0.5s
 => [3/6] COPY ./package* ./                                                                                       0.1s
 => [4/6] RUN npm ci --arch=x64 --platform=linux                                                                   3.8s
 => [5/6] COPY . .                                                                                                 0.7s
 => [6/6] RUN npm run build                                                                                        2.5s 
 => exporting to image                                                                                             1.1s 
 => => exporting layers                                                                                            1.1s
 => => writing image sha256:62ac39dcbaa9487b386c23d2593f12d2aeab47af9b0acdcf4c2898d80b4d7662                       0.0s
 => => naming to docker.io/library/logservice          
```

And then execute it:

```sh
$ docker run --rm -p 4000:4000 logservice
Running GraphQL API endpoint at http://localhost:4000/graphql
Running RESTful API endpoint at http://localhost:4000/rest
```

## Some final notes:

#### Scalability: 

While this project can be deployed to many individual instances in paralell, its impossible to scale horizontally using local filesystem for log persistence. Each instance would contain a different portion of the logs being stored. In order to scale out we'd need to use an elasticsearch instance as mentioned in the instructions, or even better, a [specialized time-series database](https://en.wikipedia.org/wiki/Time_series_database) like InfluxDb.

#### Additional features that could be included: 

- Authentication (API Key, JWT, OAuth)
- Linting (Eslint, Prettier)
- Unit tests (Jest, mocha, supertest)
- API docs (Swagger, openAPI, graphql-docs)
- CI/CD (Github actions, Gitlab runners etc)
- Metrics and Observability (Prometheus, Datadog, New Relic)

#### Potential flaws:

Apart from the complete lack of security. An important consideration in terms of performance is that the logging service should ingest BULK input (ie thosands of logs in a single request) rather than entering one log at a time, which is extremely inefficient, both from TCP/networking latency point of view, and from unpacking each HTTP request and marshalling the data to the right destination.

Also, if this service is aimed at ingesting large amounts of data, we should remove GraphQL entirely and simply pipe the raw http request body into the destination file or storage medium, using [streaming](https://nodejs.org/api/stream.html). 

The reason for this is to avoid the overhead of packing and unpacking (serializing and deserializing) the data being moved around. Every time we have to convert to/from JSON, or to/from GraphQL query format, or create any in-memory variable that we dont need to, we end up using unnecessary memory and CPU cycles. Hence slowing down the whole process, and requiring more machines to handle it.

Compare this:

(origin) raw logs -> http (text) stream -> tcp packets -> pipe to destination file (target)

To this:

(origin) raw logs -> convert to JSON -> convert to GraphQL -> http (json) stream -> tcp packets -> deserialize (json) request -> unpack graphql -> memory variable storage -> push data to destination file (target)
