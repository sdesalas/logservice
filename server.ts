import express from 'express'
import { graphqlHTTP } from 'express-graphql'
import { buildSchema } from 'graphql'
import LoggingService from './services/logging'

const loggingService = new LoggingService();
const schema = buildSchema(loggingService.graphQLschema);
const app = express()

app.use(
  '/graphql',
  graphqlHTTP({
    schema: schema,
    rootValue: loggingService,
    graphiql: true,
  })
)
app.get('/rest/logs', async (req, res) => {
  console.log('GET /rest/logs')
  res.setHeader('content-type', 'text/plain')
  await loggingService.getStream(res)
  res.status(200)
})
app.post('/rest/logs', async (req, res) => {
  console.log('POST /rest/logs')
  res.setHeader('content-type', 'text/plain')
  await loggingService.saveStream(req, res)
  res.status(200)
})

app.listen(4000)
console.log('Running GraphQL API endpoint at http://localhost:4000/graphql')
console.log('Running RESTful API endpoint at http://localhost:4000/rest')

