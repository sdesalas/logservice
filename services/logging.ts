
import stream from 'node:stream'
import os from 'os'
import fs from 'fs'

// 2023-10-27.log
const LOGFILE = `${new Date().toISOString().substring(0, 10)}.log`

interface LogData {
  logs: string[]
}

export default class LoggingService {
  public graphQLschema = `
    type Query {
      saveLogs(logs: [String]): [String],
      getLogs: [String]
    }
  `
  public async saveLogs (data: LogData): Promise<string[]> {
    console.log('graphql: saveLogs()')
    await fs.promises.appendFile(LOGFILE, data.logs.join(os.EOL) + os.EOL) 
    return data.logs
  }
  public async getLogs () : Promise<string[]> {
    console.log('graphql: getLogs()')
    const file = await fs.promises.readFile(LOGFILE)
    const logs = file.toString().split(os.EOL)
    return logs
  }
  // Optimized streaming functionality (RESTful endpoints only)
  public saveStream(input: stream.Readable, output: stream.Writable) : Promise<void> {
    const stream = fs.createWriteStream(LOGFILE, {flags: 'a'})
    return new Promise((resolve) => {
      input.pipe(stream)
      input.pipe(output)
      input.on('close', () => {
        fs.promises.appendFile(LOGFILE, os.EOL)
        resolve()
      })
    });
  }
  // Optimized streaming functionality (RESTful endpoints only)
  public getStream(output: stream.Writable) : Promise<void> {
    fs.createReadStream(LOGFILE).pipe(output)
    return Promise.resolve()
  }
}

